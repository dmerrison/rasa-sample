#!/usr/bin/env node
import 'source-map-support/register';
import * as cdk from '@aws-cdk/core';
import { RasaExperimentStack } from '../lib/rasa-experiment-stack';

const app = new cdk.App();
new RasaExperimentStack(app, 'RasaExperimentStack');
