import * as cdk from '@aws-cdk/core';
import { ApplicationLoadBalancer, ApplicationProtocol } from '@aws-cdk/aws-elasticloadbalancingv2';
import { Vpc, InstanceType, InstanceClass, InstanceSize, MachineImage, LookupMachineImage } from '@aws-cdk/aws-ec2';
import { AutoScalingGroup } from '@aws-cdk/aws-autoscaling';
import { Duration } from '@aws-cdk/core';


export class RasaExperimentStack extends cdk.Stack {
  constructor(scope: cdk.Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, {
      env: {
        region: "eu-west-2",
      }
    });

    const vpc = new Vpc(this, "RasaVPC", {
      natGateways: 1,
    })

    const lb = new ApplicationLoadBalancer(this, "RasaLoadBalancer", {
      vpc: vpc,
      internetFacing: true,

    })

    const listener = lb.addListener("RasaListener", {
      port: 80,
      open: true,

    })

    // Create an AutoScaling group and add it as a load balancing
    // target to the listener.
    const asg = new AutoScalingGroup(this, "RasaAutoScalingGroup", {
      instanceType: InstanceType.of(InstanceClass.T3, InstanceSize.MEDIUM),
      machineImage: MachineImage.genericLinux({
        "eu-west-2": "ami-043e863a972b5b693"
      }),
      vpc: vpc,
      desiredCapacity: 2,
      minCapacity: 1,
      maxCapacity: 2,
    })

    listener.addTargets('RasaCore', {
        port: 5005,
        protocol: ApplicationProtocol.HTTP,
        stickinessCookieDuration: Duration.seconds(600),
        targets: [asg]
    });
  }
}
